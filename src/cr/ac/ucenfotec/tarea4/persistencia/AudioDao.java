package cr.ac.ucenfotec.tarea4.persistencia;

import cr.ac.ucenfotec.tarea4.bl.entidades.Audio;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class AudioDao {
    Connection cnx;
    private final String TEMPLATE_INSERTAR = "insert into tmaterialaudio (idAudio,fechaCompra,restringido,tema,formato," +
            "duracion,idioma)" + "values (?,?,?,?,?,?,?)";


    private PreparedStatement comandoInsertar;
    private final String TEMPLATE_CONSULTA = "select * from tmaterialaudio";
    private PreparedStatement comandoConsulta;


    public AudioDao(Connection cnx) {
        this.cnx = cnx;
        try {
            this.comandoConsulta = cnx.prepareStatement(TEMPLATE_CONSULTA);
            this.comandoInsertar = cnx.prepareStatement(TEMPLATE_INSERTAR);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void save(Audio nuevoAudio) throws SQLException {
        LocalDate fecha = nuevoAudio.getFechaCompra();
        Date fechaSql = Date.valueOf(fecha);
        if (this.comandoInsertar != null) {
            this.comandoInsertar.setString(1, nuevoAudio.getId());
            this.comandoInsertar.setDate(2, fechaSql);
            this.comandoInsertar.setString(3, nuevoAudio.getRestringido());
            this.comandoInsertar.setString(4, nuevoAudio.getTema());
            this.comandoInsertar.setString(5, nuevoAudio.getFormato());
            this.comandoInsertar.setString(6, nuevoAudio.getDuracion());
            this.comandoInsertar.setString(7, nuevoAudio.getIdioma());
            this.comandoInsertar.execute();
        }


    }

    public List<Object> obtenerAudios() {
        ArrayList<Object> listaMateriales = new ArrayList<>();
        try {
            ResultSet resultSet = this.comandoConsulta.executeQuery();
            while (resultSet.next()) {
                Audio nuevoAudio = new Audio();
                nuevoAudio.setId(resultSet.getString("idAudio"));
                nuevoAudio.setFechaCompra(resultSet.getDate("fechaCompra").toLocalDate());
                nuevoAudio.setRestringido(resultSet.getString("restringido"));
                nuevoAudio.setTema(resultSet.getString("tema"));
                nuevoAudio.setFormato(resultSet.getString("formato"));
                nuevoAudio.setDuracion(resultSet.getString("duracion"));
                nuevoAudio.setIdioma(resultSet.getString("idioma"));
                listaMateriales.add(nuevoAudio);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaMateriales;
    }

}
