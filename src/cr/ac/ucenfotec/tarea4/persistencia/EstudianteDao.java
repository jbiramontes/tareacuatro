package cr.ac.ucenfotec.tarea4.persistencia;



import cr.ac.ucenfotec.tarea4.bl.entidades.Estudiante;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EstudianteDao {
    Connection cnx;
    private final String TEMPLATE_INSERTAR = "insert into testudiante (idEstudiante,nombreEstudiante,apellido,carrera,creditos)" + "values (?,?,?,?,?)";


    private PreparedStatement comandoInsertar;
    private final String TEMPLATE_CONSULTA = "select * from testudiante";
    private PreparedStatement comandoConsulta;


    public EstudianteDao(Connection cnx) {
        this.cnx = cnx;
        try {
            this.comandoConsulta = cnx.prepareStatement(TEMPLATE_CONSULTA);
            this.comandoInsertar = cnx.prepareStatement(TEMPLATE_INSERTAR);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public List<Object> obtenerEstudiantes() {
        ArrayList<Object> listaUsuarios = new ArrayList<>();
        try {
            ResultSet resultSet = this.comandoConsulta.executeQuery();
            while (resultSet.next()) {
                Estudiante nuevoEstudiante = new Estudiante();

                nuevoEstudiante.setId(resultSet.getString("idEstudiante"));
                nuevoEstudiante.setNombre(resultSet.getString("nombreEstudiante"));
                nuevoEstudiante.setApellido(resultSet.getString("apellido"));
                nuevoEstudiante.setCarrera(resultSet.getString("carrera"));
                nuevoEstudiante.setCreditos(resultSet.getInt("creditos"));
                listaUsuarios.add(nuevoEstudiante);
            }
        } catch (Exception e) {
            System.out.println("No se pudo extraer la informacion de los estudiantes intente de nuevo");
            e.printStackTrace();
        }
        return listaUsuarios;
    }

    public void save(Estudiante nuevoEstudiante) throws SQLException {
        if (this.comandoInsertar != null) {
            this.comandoInsertar.setString(1, nuevoEstudiante.getId());
            this.comandoInsertar.setString(2, nuevoEstudiante.getNombre());
            this.comandoInsertar.setString(3, nuevoEstudiante.getApellido());
            this.comandoInsertar.setString(4, nuevoEstudiante.getCarrera());
            this.comandoInsertar.setInt(5, nuevoEstudiante.getCreditos());

            this.comandoInsertar.execute();
        }


    }
/*
    public List<Administrativo> obtenerUsuarios() {
        ArrayList<Administrativo> listaUsuarios = new ArrayList<>();
        try {
            ResultSet resultSet = this.comandoConsulta.executeQuery();
            while (resultSet.next()) {
                Administrativo nuevoAdministrativo = new Administrativo();
                nuevoUsuario.setNombre(resultSet.getString("nombre"));
                nuevoUsuario.setApellidoUno(resultSet.getString("apellido_uno"));
                nuevoUsuario.setApellidoDos(resultSet.getString("apellido_dos"));
                nuevoUsuario.setIdentificacion(resultSet.getString("identificacion"));
                nuevoUsuario.setEdad(resultSet.getInt("edad"));
                nuevoUsuario.setNombreUsuario(resultSet.getString("nombreUsuario"));
                nuevoUsuario.setContrasena(resultSet.getString("contrasena"));
                nuevoUsuario.setPais(resultSet.getString("pais"));
                nuevoUsuario.setFechaNacimiento(resultSet.getDate("fechaNacimiento").toLocalDate());
                nuevoUsuario.setImagen(resultSet.getString("avatar"));
                nuevoUsuario.setCorreoUsuario(resultSet.getString("correoElectronico"));
                nuevoUsuario.setTipoUsuario(resultSet.getString("tipoUsuario"));

                listaUsuarios.add(nuevoUsuario);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaUsuarios;
    }
*/

}
