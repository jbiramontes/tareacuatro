package cr.ac.ucenfotec.tarea4.persistencia;

import cr.ac.ucenfotec.tarea4.bl.entidades.Otro;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class OtrosDao {
    Connection cnx;
    private final String TEMPLATE_INSERTAR = "insert into totrosmaterial (idotro,fechaCompra,restringido,tema,descripcion)" + "values (?,?,?,?,?)";


    private PreparedStatement comandoInsertar;
    private final String TEMPLATE_CONSULTA = "select * from totrosmaterial";
    private PreparedStatement comandoConsulta;


    public OtrosDao(Connection cnx) {
        this.cnx = cnx;
        try {
            this.comandoConsulta = cnx.prepareStatement(TEMPLATE_CONSULTA);
            this.comandoInsertar = cnx.prepareStatement(TEMPLATE_INSERTAR);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void save(Otro nuevoOtro) throws SQLException {
        LocalDate fecha = nuevoOtro.getFechaCompra();
        Date fechaSql = Date.valueOf(fecha);
        if (this.comandoInsertar != null) {
            this.comandoInsertar.setString(1, nuevoOtro.getId());
            this.comandoInsertar.setDate(2, fechaSql);
            this.comandoInsertar.setString(3, nuevoOtro.getRestringido());
            this.comandoInsertar.setString(4, nuevoOtro.getTema());
            this.comandoInsertar.setString(5, nuevoOtro.getDescripcion());
            this.comandoInsertar.execute();
        }


    }

    public List<Object> obtenerOtro() {
        ArrayList<Object> listaMateriales = new ArrayList<>();
        try {
            ResultSet resultSet = this.comandoConsulta.executeQuery();
            while (resultSet.next()) {
                Otro nuevoOtro = new Otro();
                nuevoOtro.setId(resultSet.getString("idOtro"));
                nuevoOtro.setFechaCompra(resultSet.getDate("fechaCompra").toLocalDate());
                nuevoOtro.setRestringido(resultSet.getString("restringido"));
                nuevoOtro.setTema(resultSet.getString("tema"));
                nuevoOtro.setDescripcion(resultSet.getString("descripcion"));
                listaMateriales.add(nuevoOtro);
            }
        } catch (Exception e) {
            System.out.println("No es posible listar usuarios");
        }
        return listaMateriales;
    }
}
