package cr.ac.ucenfotec.tarea4.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PrestamoDao {
    Connection cnx;
    private final String TEMPLATE_INSERTAR = "insert into tusuario (nombre,apellido_uno,apellido_dos,identificacion,edad,nombreUsuario,contrasena,pais,fechaNacimiento,avatar,correoElectronico,tipoUsuario)" + "values (?,?,?,?,?,?,?,?,?,?,?,?)";


    private PreparedStatement comandoInsertar;
    private final String TEMPLATE_CONSULTA = "select * from tusuario";
    private PreparedStatement comandoConsulta;


    public PrestamoDao(Connection cnx) {
        this.cnx = cnx;
        try {
            this.comandoConsulta = cnx.prepareStatement(TEMPLATE_CONSULTA);
            this.comandoInsertar = cnx.prepareStatement(TEMPLATE_INSERTAR);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
/*
    public void guardar(Administrativo nuevoAdministrativo) throws SQLException {
        LocalDate fecha = nuevoAdministrativo.getFechaNacimiento();
        Date fechaSql = Date.valueOf(fecha);
        if (this.comandoInsertar != null) {
            this.comandoInsertar.setString(1, nuevoUsuario.getNombre());
            this.comandoInsertar.setString(2, nuevoUsuario.getApellidoUno());
            this.comandoInsertar.setString(3, nuevoUsuario.getApellidoDos());
            this.comandoInsertar.setString(4, nuevoUsuario.getIdentificacion());
            this.comandoInsertar.setInt(5, nuevoUsuario.getEdad());
            this.comandoInsertar.setString(6, nuevoUsuario.getNombreUsuario());
            this.comandoInsertar.setString(7, nuevoUsuario.getContrasena());
            this.comandoInsertar.setString(8, nuevoUsuario.getPais());
            this.comandoInsertar.setDate(9, fechaSql);
            this.comandoInsertar.setString(10, nuevoUsuario.getImagen());
            this.comandoInsertar.setString(11, nuevoUsuario.getCorreoUsuario());
            this.comandoInsertar.setString(12, nuevoUsuario.getTipoUsuario());
            this.comandoInsertar.execute();
        }


    }

    public List<Administrativo> obtenerUsuarios() {
        ArrayList<Administrativo> listaUsuarios = new ArrayList<>();
        try {
            ResultSet resultSet = this.comandoConsulta.executeQuery();
            while (resultSet.next()) {
                Administrativo nuevoAdministrativo = new Administrativo();
                nuevoUsuario.setNombre(resultSet.getString("nombre"));
                nuevoUsuario.setApellidoUno(resultSet.getString("apellido_uno"));
                nuevoUsuario.setApellidoDos(resultSet.getString("apellido_dos"));
                nuevoUsuario.setIdentificacion(resultSet.getString("identificacion"));
                nuevoUsuario.setEdad(resultSet.getInt("edad"));
                nuevoUsuario.setNombreUsuario(resultSet.getString("nombreUsuario"));
                nuevoUsuario.setContrasena(resultSet.getString("contrasena"));
                nuevoUsuario.setPais(resultSet.getString("pais"));
                nuevoUsuario.setFechaNacimiento(resultSet.getDate("fechaNacimiento").toLocalDate());
                nuevoUsuario.setImagen(resultSet.getString("avatar"));
                nuevoUsuario.setCorreoUsuario(resultSet.getString("correoElectronico"));
                nuevoUsuario.setTipoUsuario(resultSet.getString("tipoUsuario"));

                listaUsuarios.add(nuevoUsuario);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaUsuarios;
    }
*/
}
