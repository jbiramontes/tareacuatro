package cr.ac.ucenfotec.tarea4.persistencia;

import cr.ac.ucenfotec.tarea4.bl.entidades.Video;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class VideoDao {
    Connection cnx;
    private final String TEMPLATE_INSERTAR = "insert into tmaterialvideo (idVideo,fechaCompra,restringido,tema,formato,duracion,idioma,director)" + "values (?,?,?,?,?,?,?,?)";


    private PreparedStatement comandoInsertar;
    private final String TEMPLATE_CONSULTA = "select * from tmaterialvideo";
    private PreparedStatement comandoConsulta;


    public VideoDao(Connection cnx) {
        this.cnx = cnx;
        try {
            this.comandoConsulta = cnx.prepareStatement(TEMPLATE_CONSULTA);
            this.comandoInsertar = cnx.prepareStatement(TEMPLATE_INSERTAR);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    public void save(Video nuevoVideo) throws SQLException {
        LocalDate fecha = nuevoVideo.getFechaCompra();
        Date fechaSql = Date.valueOf(fecha);
        if (this.comandoInsertar != null) {
            this.comandoInsertar.setString(1, nuevoVideo.getId());
            this.comandoInsertar.setDate(2, fechaSql);
            this.comandoInsertar.setString(3, nuevoVideo.getRestringido());
            this.comandoInsertar.setString(4, nuevoVideo.getTema());
            this.comandoInsertar.setString(5, nuevoVideo.getFormato());
            this.comandoInsertar.setString(6, nuevoVideo.getDuracion());
            this.comandoInsertar.setString(7, nuevoVideo.getIdioma());
            this.comandoInsertar.setString(8, nuevoVideo.getDirector());
            this.comandoInsertar.execute();
        }


    }

    public List<Object> obtenerAudios() {
        ArrayList<Object> listaMateriales = new ArrayList<>();
        try {
            ResultSet resultSet = this.comandoConsulta.executeQuery();
            while (resultSet.next()) {
                Video nuevoVideo = new Video();
                nuevoVideo.setId(resultSet.getString("idVideo"));
                nuevoVideo.setFechaCompra(resultSet.getDate("fechaCompra").toLocalDate());
                nuevoVideo.setRestringido(resultSet.getString("restringido"));
                nuevoVideo.setTema(resultSet.getString("tema"));
                nuevoVideo.setFormato(resultSet.getString("formato"));
                nuevoVideo.setDuracion(resultSet.getString("duracion"));
                nuevoVideo.setIdioma(resultSet.getString("idioma"));
                nuevoVideo.setDirector(resultSet.getString("director"));
                listaMateriales.add(nuevoVideo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaMateriales;
    }
}
