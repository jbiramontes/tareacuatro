package cr.ac.ucenfotec.tarea4.persistencia;

import cr.ac.ucenfotec.tarea4.bl.entidades.Texto;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class TextoDao {
    Connection cnx;
    private final String TEMPLATE_INSERTAR = "insert into tmaterialtexto (idTexto,fechaCompra,restringido,tema,titulo," +
            "nombreAutor,fechaPublicacion,numeroPaginas,idioma)" + "values (?,?,?,?,?,?,?,?,?)";


    private PreparedStatement comandoInsertar;
    private final String TEMPLATE_CONSULTA = "select * from tmaterialtexto";
    private PreparedStatement comandoConsulta;


    public TextoDao(Connection cnx) {
        this.cnx = cnx;
        try {
            this.comandoConsulta = cnx.prepareStatement(TEMPLATE_CONSULTA);
            this.comandoInsertar = cnx.prepareStatement(TEMPLATE_INSERTAR);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    public void save(Texto nuevoTexto) throws SQLException {
        LocalDate fecha = nuevoTexto.getFechaCompra();
        Date fechaSql = Date.valueOf(fecha);
        LocalDate fecha2 = nuevoTexto.getFechaPublicacion();
        Date fechaSql2 = Date.valueOf(fecha2);
        if (this.comandoInsertar != null) {
            this.comandoInsertar.setString(1, nuevoTexto.getId());
            this.comandoInsertar.setDate(2, fechaSql);
            this.comandoInsertar.setString(3, nuevoTexto.getRestringido());
            this.comandoInsertar.setString(4, nuevoTexto.getTema());
            this.comandoInsertar.setString(5, nuevoTexto.getTitulo());
            this.comandoInsertar.setString(6, nuevoTexto.getNombreAutor());
            this.comandoInsertar.setDate(7, fechaSql2);
            this.comandoInsertar.setInt(8, nuevoTexto.getNumeroPaginas());
            this.comandoInsertar.setString(9, nuevoTexto.getIdioma());
            this.comandoInsertar.execute();
        }


    }

    public List<Object> obtenerTexto() {
        ArrayList<Object> listaMateriales = new ArrayList<>();
        try {
            ResultSet resultSet = this.comandoConsulta.executeQuery();
            while (resultSet.next()) {
                Texto nuevoTexto = new Texto();
                nuevoTexto.setId(resultSet.getString("idTexto"));
                nuevoTexto.setFechaCompra(resultSet.getDate("fechaCompra").toLocalDate());
                nuevoTexto.setRestringido(resultSet.getString("restringido"));
                nuevoTexto.setTema(resultSet.getString("tema"));
                nuevoTexto.setTitulo(resultSet.getString("titulo"));
                nuevoTexto.setNombreAutor(resultSet.getString("nombreAutor"));
                nuevoTexto.setFechaPublicacion(resultSet.getDate("fechaPublicacion").toLocalDate());
                nuevoTexto.setNumeroPaginas(resultSet.getInt("numeroPaginas"));
                nuevoTexto.setIdioma(resultSet.getString("idioma"));
                listaMateriales.add(nuevoTexto);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaMateriales;
    }
}
