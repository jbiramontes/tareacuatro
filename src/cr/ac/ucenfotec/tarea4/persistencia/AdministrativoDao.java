package cr.ac.ucenfotec.tarea4.persistencia;

import cr.ac.ucenfotec.tarea4.bl.entidades.Administrativo;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AdministrativoDao {
    Connection cnx;
    private final String TEMPLATE_INSERTAR = "insert into tadministrativo (idAdministrativo,nombreAdministrativo,apellidoAdministrativo,tipoNombramiento,horasSemana)" + "values (?,?,?,?,?)";


    private PreparedStatement comandoInsertar;
    private final String TEMPLATE_CONSULTA = "select * from tadministrativo";
    private PreparedStatement comandoConsulta;


    public AdministrativoDao(Connection cnx) {
        this.cnx = cnx;
        try {
            this.comandoConsulta = cnx.prepareStatement(TEMPLATE_CONSULTA);
            this.comandoInsertar = cnx.prepareStatement(TEMPLATE_INSERTAR);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void save(Administrativo nuevoAdministrativo) throws SQLException {

        if (this.comandoInsertar != null) {
            this.comandoInsertar.setString(1, nuevoAdministrativo.getId());
            this.comandoInsertar.setString(2, nuevoAdministrativo.getNombre());
            this.comandoInsertar.setString(3, nuevoAdministrativo.getApellido());
            this.comandoInsertar.setString(4, nuevoAdministrativo.getTipoNombramiento());
            this.comandoInsertar.setDouble(5, nuevoAdministrativo.getHorasPorSemana());
            this.comandoInsertar.execute();
        }


    }

    public List<Object> obtenerAdministrativos() {
        ArrayList<Object> listaUsuarios = new ArrayList<>();
        try {
            ResultSet resultSet = this.comandoConsulta.executeQuery();
            while (resultSet.next()) {
                Administrativo nuevoAdministrativo = new Administrativo();
                nuevoAdministrativo.setId(resultSet.getString("idAdministrativo"));
                nuevoAdministrativo.setNombre(resultSet.getString("nombreAdministrativo"));
                nuevoAdministrativo.setApellido(resultSet.getString("apellidoAdministrativo"));
                nuevoAdministrativo.setHorasPorSemana(resultSet.getDouble("horasSemana"));
                nuevoAdministrativo.setTipoNombramiento(resultSet.getString("tipoNombramiento"));
                listaUsuarios.add(nuevoAdministrativo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaUsuarios;
    }

}
