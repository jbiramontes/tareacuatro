package cr.ac.ucenfotec.tarea4.persistencia;

import cr.ac.ucenfotec.tarea4.bl.entidades.Profesor;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ProfesorDao {
    Connection cnx;
    private final String TEMPLATE_INSERTAR = "insert into tprofesor (idProfesor,nombreProfesor,apellidoProfesor,tipoContrato,fechaContrato)" + "values (?,?,?,?,?)";


    private PreparedStatement comandoInsertar;
    private final String TEMPLATE_CONSULTA = "select * from tprofesor";
    private PreparedStatement comandoConsulta;


    public ProfesorDao(Connection cnx) {
        this.cnx = cnx;
        try {
            this.comandoConsulta = cnx.prepareStatement(TEMPLATE_CONSULTA);
            this.comandoInsertar = cnx.prepareStatement(TEMPLATE_INSERTAR);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    public void save(Profesor nuevoProfesor) throws SQLException {
        LocalDate fecha = nuevoProfesor.getFechaContrato();
        Date fechaSql = Date.valueOf(fecha);
        if (this.comandoInsertar != null) {
            this.comandoInsertar.setString(1, nuevoProfesor.getId());
            this.comandoInsertar.setString(2, nuevoProfesor.getNombre());
            this.comandoInsertar.setString(3, nuevoProfesor.getApellido());
            this.comandoInsertar.setString(4, nuevoProfesor.getTipoContrato());
            this.comandoInsertar.setDate(5, fechaSql);

            this.comandoInsertar.execute();
        }
    }
    public List<Object> obtenerProfesores() {
        ArrayList<Object> listaUsuarios = new ArrayList<>();
        try {
            ResultSet resultSet = this.comandoConsulta.executeQuery();
            while (resultSet.next()) {
                Profesor nuevoProfesor = new Profesor();

                nuevoProfesor.setId(resultSet.getString("idProfesor"));
                nuevoProfesor.setNombre(resultSet.getString("nombreProfesor"));
                nuevoProfesor.setApellido(resultSet.getString("apellidoProfesor"));
                nuevoProfesor.setTipoContrato(resultSet.getString("tipoContrato"));
                nuevoProfesor.setFechaContrato(resultSet.getDate("fechaContrato").toLocalDate());
                listaUsuarios.add(nuevoProfesor);
            }
        } catch (Exception e) {
            System.out.println("No se pudo extraer la informacion de los profesores intente de nuevo");
            e.printStackTrace();
        }
        return listaUsuarios;
    }

}
