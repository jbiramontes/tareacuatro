package cr.ac.ucenfotec.tarea4.bl.entidades;


public class Estudiante extends Persona {
    private String carrera;
    private int creditos;


    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public int getCreditos() {
        return creditos;
    }

    public void setCreditos(int creditos) {
        this.creditos = creditos;
    }

    public Estudiante() {
    }

    public Estudiante(String id, String nombre, String apellido, String carrera, int creditos) {
        super(id, nombre, apellido);
        this.carrera = carrera;
        this.creditos = creditos;
    }

    @Override
    public String toString() {
        return "Estudiante--" +
                "carrera='" + carrera + '\'' +
                ", creditos=" + creditos +
                " " + super.toString();
    }
}
