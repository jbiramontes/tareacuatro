package cr.ac.ucenfotec.tarea4.bl.entidades;

import java.time.LocalDate;

public abstract class  Material {
    private String id;
    private LocalDate fechaCompra;
    private String restringido;
    private String tema;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDate getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(LocalDate fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public String getRestringido() {
        return restringido;
    }

    public void setRestringido(String restringido) {
        this.restringido = restringido;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public Material() {
    }

    public Material(String id, LocalDate fechaCompra, String restringido, String tema) {
        this.id = id;
        this.fechaCompra = fechaCompra;
        this.restringido = restringido;
        this.tema = tema;
    }

    @Override
    public String toString() {
        return " " +
                "Id='" + id + '\'' +
                ", fechaCompra=" + fechaCompra +
                ", restringido='" + restringido + '\'' +
                ", tema='" + tema + '\'' ;
    }

}
