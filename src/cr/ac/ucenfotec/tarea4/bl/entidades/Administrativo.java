package cr.ac.ucenfotec.tarea4.bl.entidades;


public class Administrativo extends Persona {
    private String tipoNombramiento;
    private double horasPorSemana;

    public String getTipoNombramiento() {
        return tipoNombramiento;
    }

    public void setTipoNombramiento(String tipoNombramiento) {
        this.tipoNombramiento = tipoNombramiento;
    }

    public double getHorasPorSemana() {
        return horasPorSemana;
    }

    public void setHorasPorSemana(double horasPorSemana) {
        this.horasPorSemana = horasPorSemana;
    }

    public Administrativo() {
    }

    public Administrativo(String id, String nombre, String apellido, String tipoNombramiento, double horasPorSemana) {
        super(id, nombre, apellido);
        this.tipoNombramiento = tipoNombramiento;
        this.horasPorSemana = horasPorSemana;
    }

    @Override
    public String toString() {
        return "Administrativo-- " +
                "tipoNombramiento='" + tipoNombramiento + '\'' +
                ", horasPorSemana=" + horasPorSemana +
                ", " + super.toString();
    }


}
