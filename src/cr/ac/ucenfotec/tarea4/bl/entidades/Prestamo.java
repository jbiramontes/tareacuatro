package cr.ac.ucenfotec.tarea4.bl.entidades;

import java.time.LocalDate;

public class Prestamo {
    private LocalDate fechaInicio;
    private double costoPrestamos;
    private LocalDate fechaDevolucion;
    private double cargoPorRetraso;


    public LocalDate getFechaInicio() {
        return fechaInicio;

    }

    public void setFechaInicio(LocalDate fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public double getCostoPrestamos() {
        return costoPrestamos;
    }

    public void setCostoPrestamos(double costoPrestamos) {
        this.costoPrestamos = costoPrestamos;
    }

    public LocalDate getFechaDevolucion() {
        return fechaDevolucion;
    }

    public void setFechaDevolucion(LocalDate fechaDevolucion) {
        this.fechaDevolucion = fechaDevolucion;
    }

    public double getCargoPorRetraso() {
        return cargoPorRetraso;
    }

    public void setCargoPorRetraso(double cargoPorRetraso) {
        this.cargoPorRetraso = cargoPorRetraso;
    }
    public Prestamo() {
    }

    public Prestamo(LocalDate fechaInicio, double costoPrestamos, LocalDate fechaDevolucion, double cargoPorRetraso) {
        this.fechaInicio = fechaInicio;
        this.costoPrestamos = costoPrestamos;
        this.fechaDevolucion = fechaDevolucion;
        this.cargoPorRetraso = cargoPorRetraso;
    }

}
