package cr.ac.ucenfotec.tarea4.bl.entidades;



import java.time.LocalDate;

public class Profesor extends Persona {

    private String tipoContrato;
    private LocalDate fechaContrato;

    public String getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(String tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    public LocalDate getFechaContrato() {
        return fechaContrato;
    }

    public void setFechaContrato(LocalDate fechaContrato) {
        this.fechaContrato = fechaContrato;
    }

    public Profesor() {
    }

    public Profesor(String id, String nombre, String apellido, String tipoContrato, LocalDate fechaContrato) {
        super(id, nombre, apellido);
        this.tipoContrato = tipoContrato;
        this.fechaContrato = fechaContrato;
    }

    @Override
    public String toString() {
        return "Profesor--" +
                "tipoContrato='" + tipoContrato + '\'' +
                ", fechaContrato=" + fechaContrato +
                " " + super.toString();
    }


}
