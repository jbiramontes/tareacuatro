package cr.ac.ucenfotec.tarea4.bl.entidades;


import java.time.LocalDate;

public class Otro extends Material {
    private String descripcion;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Otro() {
    }

    public Otro(String id, LocalDate fechaCompra, String restringido, String tema, String descripcion) {
        super(id, fechaCompra, restringido, tema);
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Otro--" +
                "descripcion='" + descripcion + '\'' +
                " " + super.toString();
    }

}
