package cr.ac.ucenfotec.tarea4.bl.entidades;



import java.time.LocalDate;

public class Video extends Material {
    private String formato;
    private String duracion;
    private String Idioma;
    private String director;

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getIdioma() {
        return Idioma;
    }

    public void setIdioma(String idioma) {
        Idioma = idioma;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public Video() {
    }

    public Video(String id, LocalDate fechaCompra, String restringido, String tema, String formato, String duracion,
                 String idioma, String director) {
        super(id, fechaCompra, restringido, tema);
        this.formato = formato;
        this.duracion = duracion;
        Idioma = idioma;
        this.director = director;
    }

    @Override
    public String toString() {
        return "Video-- " +
                "formato='" + formato + '\'' +
                ", duracion='" + duracion + '\'' +
                ", Idioma='" + Idioma + '\'' +
                ", director='" + director + '\'' +
                " " + super.toString();
    }

}
