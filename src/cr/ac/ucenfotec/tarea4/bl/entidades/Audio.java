package cr.ac.ucenfotec.tarea4.bl.entidades;


import java.time.LocalDate;

public class Audio extends Material {
    private String formato;
    private String duracion;
    private String Idioma;

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getIdioma() {
        return Idioma;
    }

    public void setIdioma(String idioma) {
        Idioma = idioma;
    }

    public Audio() {
    }

    public Audio(String id, LocalDate fechaCompra, String restringido, String tema, String formato, String duracion,
                 String idioma) {
        super(id, fechaCompra, restringido, tema);
        this.formato = formato;
        this.duracion = duracion;
        this.Idioma = idioma;
    }

    @Override
    public String toString() {
        return "Audio--" +
                "formato='" + formato + '\'' +
                ", duracion='" + duracion + '\'' +
                ", Idioma='" + Idioma + '\'' +
                " " + super.toString();
    }


}
