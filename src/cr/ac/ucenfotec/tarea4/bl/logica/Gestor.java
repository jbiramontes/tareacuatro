package cr.ac.ucenfotec.tarea4.bl.logica;

import cr.ac.ucenfotec.tarea4.PropertiesHandler;
import cr.ac.ucenfotec.tarea4.bl.entidades.*;
import cr.ac.ucenfotec.tarea4.persistencia.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public class Gestor {
    private AdministrativoDao administrativoDao;
    private EstudianteDao estudianteDao;
    private ProfesorDao profesorDao;
    private AudioDao audioDao;
    private OtrosDao otrosDao;
    private PrestamoDao prestamoDao;
    private TextoDao textoDao;
    private VideoDao videoDao;

    private ArrayList<Persona> listaUsuarios = new ArrayList<>();

    private Connection conection;
    PropertiesHandler newPropertie = new PropertiesHandler();


    public Gestor() {
        try {
            newPropertie.loadProperties();
            Class.forName(newPropertie.getDriver()).newInstance();
            this.conection = DriverManager.getConnection(newPropertie.getCnxStr(), newPropertie.getUsrname(),
                    newPropertie.getPassword());
            this.administrativoDao = new AdministrativoDao(this.conection);
            this.estudianteDao = new EstudianteDao(this.conection);
            this.profesorDao = new ProfesorDao(this.conection);
            this.audioDao = new AudioDao(this.conection);
            this.otrosDao = new OtrosDao(this.conection);
            this.prestamoDao = new PrestamoDao(this.conection);
            this.textoDao = new TextoDao(this.conection);
            this.videoDao = new VideoDao(this.conection);
        } catch (Exception e) {
            System.out.println("Cant connect to db");
            System.out.println(e.getMessage());
        }
    }

    public LocalDate agregarFecha(int dia, int mes, int anio) {
        LocalDate fecha = LocalDate.of(anio, mes, dia);
        return fecha;

    }

    public LocalDate convertirFecha(String fecha) {
        try {
            return LocalDate.parse(fecha);
        } catch (Exception e) {
            System.out.println("Usted ha digitado la fecha en un formato incorrecto");

        }
        return null;
    }

    public void agregarAdministrativo(String id, String nombre, String apellido, String tipoNombramiento, double horasSemana) {
        Administrativo nuevoAdministrativo = new Administrativo(id, nombre, apellido, tipoNombramiento, horasSemana);
        try {
            administrativoDao.save(nuevoAdministrativo);
        } catch (SQLException a) {
            a.printStackTrace();

        }

    }

    public void agregarEstudiante(String id, String nombre, String apellido, String carrera, int creditos) {
        Estudiante nuevoEstudiante = new Estudiante(id, nombre, apellido, carrera, creditos);
        try {
            estudianteDao.save(nuevoEstudiante);
        } catch (SQLException a) {
            a.printStackTrace();

        }

    }

    public void agregarProfesor(String id, String nombre, String apellido, String tipoContrato, LocalDate fechaContrato) {
        Profesor nuevoProfesor = new Profesor(id, nombre, apellido, tipoContrato, fechaContrato);
        try {
            profesorDao.save(nuevoProfesor);
        } catch (SQLException a) {
            a.printStackTrace();

        }

    }

    public void agregarAudio(String id, LocalDate fechaCompra, String restringido, String tema, String formato, String duracion, String idioma) {
        Audio nuevoAudio = new Audio(id, fechaCompra, restringido, tema, formato, duracion, idioma);
        try {
            audioDao.save(nuevoAudio);
        } catch (SQLException a) {
            a.printStackTrace();

        }

    }

    public void agregarTexto(String id, LocalDate fechaCompra, String restringido, String tema, String titulo,
                             String nombreAutor, LocalDate fechaPublicacion, int numeroPaginas, String idioma) {
        Texto nuevoTexto = new Texto(id, fechaCompra, restringido, tema, titulo, nombreAutor, fechaPublicacion, numeroPaginas, idioma);
        try {
            textoDao.save(nuevoTexto);
        } catch (SQLException a) {
            a.printStackTrace();

        }

    }

    public void agregarVideo(String id, LocalDate fechaCompra, String restringido, String tema, String formato, String duracion,
                             String idioma, String director) {
        Video nuevoVideo = new Video(id, fechaCompra, restringido, tema, formato, duracion, idioma, director);
        try {
            videoDao.save(nuevoVideo);
        } catch (SQLException a) {
            a.printStackTrace();

        }

    }

    public void agregarOtro(String id, LocalDate fechaCompra, String restringido, String tema, String descripcion) {
        Otro nuevoOtro = new Otro(id, fechaCompra, restringido, tema, descripcion);
        try {
            otrosDao.save(nuevoOtro);
        } catch (SQLException a) {
            a.printStackTrace();

        }

    }

    public List<Object> listarUsuarios(int opcion) {
        if (opcion == 3) {
            return administrativoDao.obtenerAdministrativos();
        }
        if (opcion == 1) {


            return estudianteDao.obtenerEstudiantes();
        }
        if (opcion == 2) {
            return profesorDao.obtenerProfesores();
        }
        return null;
    }

    public List<Object> listarMaterial(int opcion) {
        if (opcion == 3) {
            return videoDao.obtenerAudios();
        }
        if (opcion == 1) {

            return audioDao.obtenerAudios();
        }
        if (opcion == 2) {
            return textoDao.obtenerTexto();
        }
        if (opcion == 4) {
            return otrosDao.obtenerOtro();
        }
        return null;
    }


}
