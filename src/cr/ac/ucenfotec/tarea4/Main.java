package cr.ac.ucenfotec.tarea4;

import cr.ac.ucenfotec.tarea4.controlador.Controlador;

/**+
 * @author Jorge Biramontes Calvo
 * @version 1
 * @Tarea #4 Programacion orientada a objetos
 */
public class Main {

    public static void main(String[] args) {
        Controlador ejecutar= new Controlador();
        ejecutar.ejecutarPrograma();

    }
}
