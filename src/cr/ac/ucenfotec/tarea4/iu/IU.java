package cr.ac.ucenfotec.tarea4.iu;

import java.io.PrintStream;
import java.util.Scanner;

public class IU {
    private PrintStream output = new PrintStream(System.out);
    private Scanner input = new Scanner(System.in).useDelimiter("\n");


    public void mostrarMenuPrincipal() {

        output.println("Bienvenido: escoja una opcion ");

        output.println("1. Registrar usuario");
        output.println("2. Registrar Material");
        output.println("3. Listar usuarios ");
        output.println("4. Listar materiales");
        output.println("5. Prestamos *NO FUNCIONAL*");

        output.println("0. Salir");

    }

    public void mostrarMenuRegistroUsuario() {

        output.println("Seleccione tipo usuario a registrar ");

        output.println("1. Administrativo ");
        output.println("2. Estudiante ");
        output.println("3. Profesor ");
        output.println("0. Salir");

    }

    public void mostrarMenuListarUsuario() {

        output.println("Seleccione tipo usuario a listar ");
        output.println("1. Estudiante ");
        output.println("2. Profesor");
        output.println("3. Administrativo ");
        output.println("0. Salir");

    }

    public void mostrarMenuMaterial() {

        output.println("Seleccione el tipo de material a registrar ");

        output.println("1. Audio");
        output.println("2. Texto");
        output.println("3. Video ");
        output.println("4. Otro");
        output.println("0. Salir");

    }

    public int leerNumero() {

        try {
            return input.nextInt();

        } catch (Exception e) {
            int numero = 0;
            System.out.println("Ha digitado letras en entrada de numeros intente nuevamente");
            return numero;
        }

    }

    public void imprimirMensaje(String mensaje) {

        output.println(mensaje);

    }

    public String leerTexto() {
        return input.next();


    }

    public double leerHorasTrabajadas() {
        double numero = 1;
        do {


            try {
                return input.nextDouble();

            } catch (Exception e) {

                numero = 0;
                System.out.println("Ha digitado un dato incorrecto");
                return numero;
            }
        } while (numero == 0);
    }


}