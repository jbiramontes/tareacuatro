package cr.ac.ucenfotec.tarea4.controlador;

import cr.ac.ucenfotec.tarea4.bl.logica.Gestor;
import cr.ac.ucenfotec.tarea4.iu.IU;

import java.time.LocalDate;
import java.util.List;

public class Controlador {
    private IU interfaz = new IU();
    private Gestor gestor = new Gestor();

    public void ejecutarPrograma() {

        int opcion = 0;
        do {
            interfaz.mostrarMenuPrincipal();
            opcion = interfaz.leerNumero();
            procesarOpcion(opcion);
        } while (opcion != 0);


    }

    private void procesarOpcion(int opcion) {
        switch (opcion) {

            case 1:
                interfaz.mostrarMenuRegistroUsuario();
                int nuevaopcion = interfaz.leerNumero();
                crearUsuario(nuevaopcion);
                break;
            case 2:
                interfaz.mostrarMenuMaterial();
                int nuevaOpcion = interfaz.leerNumero();
                crearMaterial(nuevaOpcion);
                break;
            case 3:
                interfaz.mostrarMenuListarUsuario();
                int nuevaOpcionUno = interfaz.leerNumero();
                List<Object> lista = gestor.listarUsuarios(nuevaOpcionUno);
                for (Object e : lista) {
                    System.out.println(e.toString());
                }
                break;
            case 4:
                interfaz.mostrarMenuMaterial();
                int nuevaOpcionDos = interfaz.leerNumero();

                List<Object> listaMaterial = gestor.listarMaterial(nuevaOpcionDos);

                if (listaMaterial.size() == 0) {
                    System.out.println("No hay materiales de este tipo registrados");

                } else {
                    for (Object e : listaMaterial) {
                        System.out.println(e.toString());
                    }
                }
                break;
            case 0:
                interfaz.imprimirMensaje("Te esperamos pronto");
                break;
            default:
                interfaz.imprimirMensaje("Opcion desconocida");
        }

    }

    private void crearMaterial(int opcion) {
        interfaz.imprimirMensaje("Identificacion del material");
        String id = interfaz.leerTexto();
        interfaz.imprimirMensaje("Fecha de compra YYYY-MM-DD");
        String fecha = interfaz.leerTexto();
        LocalDate fechaCompra = gestor.convertirFecha(fecha);
        interfaz.imprimirMensaje("Restringido?");
        String restringido = interfaz.leerTexto();
        interfaz.imprimirMensaje("Tema");
        String tema = interfaz.leerTexto();

        if (opcion == 1) {
            interfaz.imprimirMensaje("Formato ");
            String formato = interfaz.leerTexto();
            interfaz.imprimirMensaje("Duracion");
            String duracion = interfaz.leerTexto();
            interfaz.imprimirMensaje("Idioma");
            String idioma = interfaz.leerTexto();

            gestor.agregarAudio(id, fechaCompra, restringido, tema, formato, duracion, idioma);
            interfaz.imprimirMensaje("Material ha sido registrado");
        }

        if (opcion == 2) {
            interfaz.imprimirMensaje("Titulo libro ");
            String titulo = interfaz.leerTexto();
            interfaz.imprimirMensaje("Nombre autor");
            String nombreAutor = interfaz.leerTexto();
            interfaz.imprimirMensaje("Fecha de publicacion YYYY-MM-DD");
            String fechaPublicacionLibro = interfaz.leerTexto();
            LocalDate fechaPublicacion = gestor.convertirFecha(fechaPublicacionLibro);
            interfaz.imprimirMensaje("Numero de paginas");
            int paginas = interfaz.leerNumero();
            interfaz.imprimirMensaje("Idioma ");
            String idioma = interfaz.leerTexto();
            gestor.agregarTexto(id, fechaCompra, restringido, tema, titulo, nombreAutor, fechaPublicacion, paginas, idioma);
            interfaz.imprimirMensaje("Material ha sido registrado");
        }
        if (opcion == 3) {
            interfaz.imprimirMensaje("Formato ");
            String formato = interfaz.leerTexto();
            interfaz.imprimirMensaje("Duracion");
            String duracion = interfaz.leerTexto();
            interfaz.imprimirMensaje("Idioma");
            String idioma = interfaz.leerTexto();
            interfaz.imprimirMensaje("Director ");
            String director = interfaz.leerTexto();

            gestor.agregarVideo(id, fechaCompra, restringido, tema, formato, duracion, idioma, director);
            interfaz.imprimirMensaje("Material ha sido registrado");
        }
        if (opcion == 4) {
            interfaz.imprimirMensaje("Descripcion");
            String descripcion = interfaz.leerTexto();
            gestor.agregarOtro(id, fechaCompra, restringido, tema, descripcion);
            interfaz.imprimirMensaje("Material ha sido registrado");
        }

    }

    private void crearUsuario(int opcion) {
        try {
            interfaz.imprimirMensaje("Identificacion");
            String id = interfaz.leerTexto();
            interfaz.imprimirMensaje("Nombre");
            String nombre = interfaz.leerTexto();
            interfaz.imprimirMensaje("Apellido");
            String apellido = interfaz.leerTexto();
            if (opcion == 1) {
                interfaz.imprimirMensaje("Tipo nombramiento");
                String tipoNombramiento = interfaz.leerTexto();
                interfaz.imprimirMensaje("Horas semanales");
                String horas = interfaz.leerTexto();
                Double horasSemana = Double.parseDouble(horas);
                gestor.agregarAdministrativo(id, nombre, apellido, tipoNombramiento, horasSemana);
                interfaz.imprimirMensaje("Usuario ha sido registrado");
            }
            if (opcion == 2) {
                interfaz.imprimirMensaje("Ingrese la carrera ");
                String carrera = interfaz.leerTexto();
                interfaz.imprimirMensaje("Digite el numero de creditos matriculados");
                int creditos = interfaz.leerNumero();
                gestor.agregarEstudiante(id, nombre, apellido, carrera, creditos);
                interfaz.imprimirMensaje("Usuario ha sido registrado");
            }
            if (opcion == 3) {
                interfaz.imprimirMensaje("Fecha contrato");
                interfaz.imprimirMensaje("Dia");
                int dia = interfaz.leerNumero();
                interfaz.imprimirMensaje("Mes");
                int mes = interfaz.leerNumero();
                interfaz.imprimirMensaje("Año");
                int anio = interfaz.leerNumero();
                interfaz.imprimirMensaje("Tipo de contrato");
                String tipoContrato = interfaz.leerTexto();
                LocalDate fecha = gestor.agregarFecha(dia, mes, anio);
                gestor.agregarProfesor(id, nombre, apellido, tipoContrato, fecha);
                interfaz.imprimirMensaje("Usuario ha sido registrado");
            }
        } catch (NumberFormatException e) {
            System.out.println("Existen datos ingresados con formato incorrecto");
            System.out.println("Usuario no se pudo registrar");

        }
    }
}